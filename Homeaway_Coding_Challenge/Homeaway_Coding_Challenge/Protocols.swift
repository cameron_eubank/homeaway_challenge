//
//  Protocols.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

// MARK: ViewModelConfigurable
protocol ViewModelConfigurable: class {
    associatedtype ViewModel
    func configure(withViewModel viewModel: ViewModel)
}

// MARK: SectionRepresentable
protocol SectionRepresentable {
    associatedtype Row
    var rows: [Row] { get }
}
