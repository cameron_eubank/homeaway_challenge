//
//  EventFavoriteManager.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

struct EventFavoriteManager {
    
    private let defaults = UserDefaults.standard
    
    func isFavorited(event: Event) -> Bool {
        guard let value = defaults.value(forKey: event.key) as? Bool else {
            return false
        }
        return value
    }
    
    func favorite(event: Event) {
        defaults.set(true, forKey: event.key)
    }
}

fileprivate extension Event {
    var key: String {
        return "\(self.id)"
    }
}
