//
//  EventDetailTableViewModel.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

protocol EventDetailTableViewModelDelegate: class {
    func didFavorite(event: Event)
}

enum EventDetailTableViewCellType {
    case display(EventTableViewCellViewModel)
}

struct EventDetailTableViewSection: SectionRepresentable {
    var rows: [EventDetailTableViewCellType] = []
}

class EventDetailTableViewModel {
    
    let event: Event
    fileprivate var sections: [EventDetailTableViewSection] = []
    private weak var delegate: EventDetailTableViewModelDelegate?
    fileprivate let favoritedEventManager = EventFavoriteManager()
    
    init(event: Event, delegate: EventDetailTableViewModelDelegate) {
        self.event = event
        self.delegate = delegate
        self.sections = sections(fromEvent: event)
    }
    
    private func sections(fromEvent event: Event) -> [EventDetailTableViewSection] {
        return [
            EventDetailTableViewSection(rows: [
                .display(EventTableViewCellViewModel(event: event))
            ])
        ]
    }
}

// MARK: VC/VM Interaction
extension EventDetailTableViewModel {
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func favoriteEvent(completion: () -> ()) {
        guard !favoritedEventManager.isFavorited(event: event) else { return }
        favoritedEventManager.favorite(event: event)
        delegate?.didFavorite(event: event)
        completion()
    }
    
    func cellType(atIndexPath indexPath: IndexPath) -> EventDetailTableViewCellType {
        return sections[indexPath.section].rows[indexPath.row]
    }
}
