//
//  EventSearchTableViewModel.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

protocol EventSearchTableViewModelDelegate: class {
    func networkActivityDidBegin()
    func networkActivityDidFinish()
    func searchResultsDidChange()
    func wasProvided(event: Event)
}

enum EventSearchTableViewCellType {
    case event(EventTableViewCellViewModel)
}

struct EventSearchTableViewSection: SectionRepresentable {
    var rows: [EventSearchTableViewCellType] = []
}

fileprivate struct Constants {
    struct DisplayText {
        static let title = "Events"
    }
}

class EventSearchTableViewModel {
    
    let title: String = Constants.DisplayText.title
    private weak var delegate: EventSearchTableViewModelDelegate?
    private var sections: [EventSearchTableViewSection] = []
    
    private func sections(fromEvents events: [Event]) -> [EventSearchTableViewSection] {
        let sections = events.compactMap {
            return EventSearchTableViewSection(rows: [.event(EventTableViewCellViewModel(event: $0))])
        }
        return sections
    }
}

// MARK: Searching
extension EventSearchTableViewModel {
    
    func search(forEvent keyword: String) {
        guard !keyword.isEmpty else {
            handleEmptyKeywordSearch()
            return
        }
        handleSearch(forKeyword: keyword)
    }
    
    private func handleEmptyKeywordSearch() {
        sections = []
        delegate?.searchResultsDidChange()
    }
    
    private func handleSearch(forKeyword keyword: String) {
        let service = EventSearchService()
        delegate?.networkActivityDidBegin()
        service.requestEvents(fromKeyword: keyword) { response in
            DispatchQueue.main.async {
                self.delegate?.networkActivityDidFinish()
            }
            switch response {
            case .success(let events):
                DispatchQueue.main.async {
                    self.sections = self.sections(fromEvents: events)
                    self.delegate?.searchResultsDidChange()
                }
            case .failure:
                print("Something went wrong")
            }
        }
    }
}

// MARK: VC/VM Interaction
extension EventSearchTableViewModel {
    
    func bind(withDelegate delegate: EventSearchTableViewModelDelegate) {
        self.delegate = delegate
    }
    
    func numberOfSections() -> Int {
        return sections.count
    }
    
    func numberOfRows(inSection section: Int) -> Int {
        return sections[section].rows.count
    }
    
    func didSelectRow(atIndexPath indexPath: IndexPath) {
        switch cellType(atIndexPath: indexPath) {
        case .event(let eventCellViewModel):
            let event = eventCellViewModel.event
            delegate?.wasProvided(event: event)
        }
    }
    
    func cellType(atIndexPath indexPath: IndexPath) -> EventSearchTableViewCellType {
        return sections[indexPath.section].rows[indexPath.row]
    }
}
