//
//  EventDetailDisplayTableViewCell.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import UIKit

class EventDetailDisplayTableViewCell: UITableViewCell {
    
    static func nib() -> UINib {
        return UINib(nibName: "EventDetailDisplayTableViewCell", bundle: nil)
    }
    
    @IBOutlet var eventImageView: UIImageView!
    @IBOutlet var eventTimeLabel: UILabel!
    @IBOutlet var eventLocationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        eventImageView.layer.cornerRadius = eventImageView.bounds.size.height * 0.05
        eventImageView.clipsToBounds = true
    }
}

// MARK: ViewModelConfigurable
extension EventDetailDisplayTableViewCell: ViewModelConfigurable {
    
    func configure(withViewModel viewModel: EventTableViewCellViewModel) {
        self.eventTimeLabel.text = viewModel.displayTime
        self.eventLocationLabel.text = viewModel.event.location
        self.eventImageView?.image = UIImage(named: "question") // Sets a default image.
        ImageCache.shared.image(fromUrl: viewModel.event.imageUrl) { response in
            DispatchQueue.main.async {
                switch response {
                case .success(let image):
                    self.eventImageView.image = image
                case .failure(_): return
                }
            }
        }
    }
}
