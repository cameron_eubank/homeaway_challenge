//
//  EventSearchResultTableViewCell.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import UIKit

class EventSearchResultTableViewCell: UITableViewCell {
    
    static func nib() -> UINib {
        return UINib(nibName: "EventSearchResultTableViewCell", bundle: nil)
    }
    
    @IBOutlet var eventFavoritedImageView: UIImageView!
    @IBOutlet var eventImageView: UIImageView!
    @IBOutlet var eventTitleLabel: UILabel!
    @IBOutlet var eventLocationLabel: UILabel!
    @IBOutlet var eventTimeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        separatorInset = UIEdgeInsetsMake(0, 0, 0, 0)
        eventImageView.layer.cornerRadius = eventImageView.bounds.size.width * 0.1
        eventImageView.clipsToBounds = true
    }
}

// MARK: ViewModelConfigurable
extension EventSearchResultTableViewCell: ViewModelConfigurable {
    
    func configure(withViewModel viewModel: EventTableViewCellViewModel) {
        self.eventTitleLabel.text = viewModel.event.title
        self.eventLocationLabel.text = viewModel.event.location
        self.eventTimeLabel.text = viewModel.displayTime
        self.eventImageView?.image = UIImage(named: "question") // Sets a default image.
        self.eventFavoritedImageView?.image = eventFavoritedImageViewImage(viewModel: viewModel)
        ImageCache.shared.image(fromUrl: viewModel.event.imageUrl) { response in
            DispatchQueue.main.async {
                switch response {
                case .success(let image):
                    self.eventImageView.image = image
                case .failure(_): return
                }
            }
        }
    }
    
    private func eventFavoritedImageViewImage(viewModel: EventTableViewCellViewModel) -> UIImage? {
        guard viewModel.favoritedEventManager.isFavorited(event: viewModel.event) else { return nil }
        return UIImage(named: "favorited")
    }
}
