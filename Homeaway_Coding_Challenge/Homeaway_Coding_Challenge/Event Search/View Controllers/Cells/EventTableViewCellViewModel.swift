//
//  EventTableViewCellViewModel.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

struct EventTableViewCellViewModel {
    
    let favoritedEventManager = EventFavoriteManager()
    let event: Event
    var displayTime: String? {
        guard let date = dateFormatter.date(from: event.time) else { return nil }
        return date.description(with: .current)
    }
    
    private var dateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }
}
