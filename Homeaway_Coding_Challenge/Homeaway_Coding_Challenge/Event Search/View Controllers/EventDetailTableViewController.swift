//
//  EventDetailTableViewController.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    struct ReuseID {
        static let eventDetailDisplayCell = "event_detail_display_cell"
    }
}

class EventDetailTableViewController: UITableViewController {
    
    fileprivate let tableViewModel: EventDetailTableViewModel
    
    init(withTableViewModel tableViewModel: EventDetailTableViewModel) {
        self.tableViewModel = tableViewModel
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    private func configure() {
        configureTableView()
        configureNavigationController()
        title = tableViewModel.event.title
    }
    
    private func configureTableView() {
        tableView.register(EventDetailDisplayTableViewCell.nib(), forCellReuseIdentifier: Constants.ReuseID.eventDetailDisplayCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.tableFooterView = UIView() // Removes extraneous cell separators.
    }
    
    private func configureNavigationController() {
        navigationItem.setRightBarButton(favoriteStateBarButtonItem, animated: true)
    }
    
    private var favoriteStateBarButtonItem: UIBarButtonItem {
        let favoriteManager = EventFavoriteManager()
        if favoriteManager.isFavorited(event: tableViewModel.event) {
            return UIBarButtonItem(image: UIImage(named: "favorited")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(favoriteEvent))
        } else {
            return UIBarButtonItem(image: UIImage(named: "unfavorited")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: #selector(favoriteEvent))
        }
    }
    
    @objc private func favoriteEvent() {
        tableViewModel.favoriteEvent {
            self.configureNavigationController()
        }
    }

    // MARK: UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewModel.numberOfSections()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewModel.numberOfRows(inSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableViewModel.cellType(atIndexPath: indexPath) {
        case .display(let displayCellViewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseID.eventDetailDisplayCell) as? EventDetailDisplayTableViewCell else { break }
            cell.configure(withViewModel: displayCellViewModel)
            return cell
        }
        return UITableViewCell()
    }
}
