//
//  EventSearchTableViewController.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import UIKit

fileprivate struct Constants {
    struct ReuseID {
        static let eventSearchResultCell = "event_search_result_cell"
    }
}

class EventSearchTableViewController: UITableViewController {
    
    private let tableViewModel = EventSearchTableViewModel()
    private let searchController = UISearchController(searchResultsController: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewModel.bind(withDelegate: self)
        configure()
    }
    
    private func configure() {
        configureSearchController()
        configureTableView()
        configureNavigationController()
        title = tableViewModel.title
    }
    
    private func configureSearchController() {
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.searchBar.sizeToFit()
        searchController.searchBar.tintColor = Colors.navigationBarTint
        searchController.searchBar.delegate = self
        definesPresentationContext = true
    }
    
    private func configureTableView() {
        tableView.register(EventSearchResultTableViewCell.nib(), forCellReuseIdentifier: Constants.ReuseID.eventSearchResultCell)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        tableView.tableFooterView = UIView() // Removes extraneous cell separators.
    }
    
    private func configureNavigationController() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.barTintColor = Colors.navigationBar
        navigationController?.navigationBar.tintColor = Colors.navigationBarTint
        navigationItem.titleView = searchController.searchBar
    }
    
    // MARK: UITableViewDataSource
    override func numberOfSections(in tableView: UITableView) -> Int {
        return tableViewModel.numberOfSections()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewModel.numberOfRows(inSection: section)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch tableViewModel.cellType(atIndexPath: indexPath) {
        case .event(let eventCellViewModel):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: Constants.ReuseID.eventSearchResultCell) as? EventSearchResultTableViewCell else { break }
            cell.configure(withViewModel: eventCellViewModel)
            return cell
        }
        return UITableViewCell()
    }
    
    // MARK: UITableViewDelegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewModel.didSelectRow(atIndexPath: indexPath)
    }
}

// MARK: UISearchBarDelegate
extension EventSearchTableViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchBarText = searchController.searchBar.text else { return }
        tableViewModel.search(forEvent: searchBarText)
    }
}

// MARK: EventSearchTableViewModelDelegate
extension EventSearchTableViewController: EventSearchTableViewModelDelegate {
    
    func searchResultsDidChange() {
        tableView.reloadData()
    }
    
    func wasProvided(event: Event) {
        let detailTableViewModel = EventDetailTableViewModel(event: event, delegate: self)
        let detailTableViewController = EventDetailTableViewController(withTableViewModel: detailTableViewModel)
        navigationController?.pushViewController(detailTableViewController, animated: true)
    }
    
    func networkActivityDidBegin() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func networkActivityDidFinish() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}

// MARK: EventDetailTableViewModelDelegate
extension EventSearchTableViewController: EventDetailTableViewModelDelegate {
    
    func didFavorite(event: Event) {
        tableView.reloadData()
    }
}
