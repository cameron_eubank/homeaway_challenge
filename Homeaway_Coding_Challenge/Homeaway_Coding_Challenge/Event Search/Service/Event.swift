//
//  Event.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

fileprivate struct Key {
    static let id = "id"
    static let title = "short_title"
    static let time = "datetime_local"
    static let venue = "venue"
    static let location = "display_location"
    static let performers = "performers"
    static let images = "images"
    static let imageUrl = "huge"
}

struct Event {
    let id: Int
    let title: String
    let time: String
    let location: String
    let imageUrl: URL
}

extension Event {
    
    init?(fromJSON JSON: [String: Any]) {
        guard let id = JSON[Key.id] as? Int,
            let title = JSON[Key.title] as? String,
            let time = JSON[Key.time] as? String,
            let venue = JSON[Key.venue] as? [String: Any],
            let location = venue[Key.location] as? String,
            let performers = JSON[Key.performers] as? [[String: Any]],
            let images = performers.first?[Key.images] as? [String: Any],
            let imageString = images[Key.imageUrl] as? String,
            let imageUrl = URL(string: imageString)
        else {
                return nil
        }
        self.id = id
        self.title = title
        self.time = time
        self.location = location
        self.imageUrl = imageUrl
    }
}
