//
//  EventSearchResponse.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

fileprivate struct Key {
    static let events = "events"
}

enum EventSearchResponse {
    
    case failure(Error?)
    case success([Event])
    
    init(JSON: [String: Any]?, error: Error?) {
        guard let JSON = JSON, let eventsJSON = JSON[Key.events] as? [[String: Any]], error == nil else {
            self = .failure(error)
            return
        }
        let events: [Event] = eventsJSON.compactMap { return Event(fromJSON: $0) }
        self = .success(events)
    }
}
