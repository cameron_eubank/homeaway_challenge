//
//  EventSearchService.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/21/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation

fileprivate struct Constants {
    private static let base = "https://api.seatgeek.com/2/events?client_id="
    private static let apiKey = "OTk3ODAxM3wxNTI5NjM0OTI4Ljky"
    private static let query = "&q="
    static let eventSearchURL = "\(base)\(apiKey)\(query)"
}

struct EventSearchService {
    
    func requestEvents(fromKeyword keyword: String, completion: @escaping (EventSearchResponse) -> ()) {
        guard let url = encodedUrl(fromKeyword: keyword) else {
            completion(EventSearchResponse(JSON: nil, error: nil))
            return
        }
        URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
            if let error = error {
                completion(EventSearchResponse(JSON: nil, error: error))
            }
            guard let data = data else { return }
            do {
                guard let JSON = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
                    completion(EventSearchResponse(JSON: nil, error: nil))
                    return
                }
                completion(EventSearchResponse(JSON: JSON, error: nil))
            } catch let error {
                completion(EventSearchResponse(JSON: nil, error: error))
            }
        }).resume()
    }
    
    private func encodedUrl(fromKeyword keyword: String) -> URL? {
        let raw = "\(Constants.eventSearchURL)\(keyword)"
        guard let encoded = raw.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else { return nil }
        return URL(string: encoded)
    }
}
