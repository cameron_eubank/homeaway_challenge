//
//  ImageCacheResponse.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation
import UIKit

enum ImageCacheResponse {
    
    case success(UIImage)
    case failure(Error?)
    
    init(image: UIImage?, error: Error?) {
        guard let image = image else {
            self = .failure(error)
            return
        }
        self = .success(image)
    }
}

