//
//  ImageCache.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation
import UIKit

class ImageCache {
    
    static let shared = ImageCache()
    private let cache = NSCache<NSString, UIImage>()
    
    func image(fromUrl url: URL, completion: @escaping (ImageCacheResponse) -> ()) {
        if let image = cache.object(forKey: url.absoluteString as NSString) {
            completion(ImageCacheResponse(image: image, error: nil))
        } else {
            URLSession.shared.dataTask(with: url, completionHandler: { (data, _, error) in
                guard let data = data, let image = UIImage(data: data) else {
                    completion(ImageCacheResponse(image: nil, error: error))
                    return
                }
                completion(ImageCacheResponse(image: image, error: nil))
            }).resume()
        }
    }
}
