//
//  Colors.swift
//  Homeaway_Coding_Challenge
//
//  Created by Cameron Eubank on 6/24/18.
//  Copyright © 2018 Cameron Eubank. All rights reserved.
//

import Foundation
import UIKit

struct Colors {
    static let navigationBar: UIColor = UIColor(r: 9, g: 132, b: 227)
    static let navigationBarTint: UIColor = .white
}

extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat){
        self.init(red: r/255, green: g/255, blue: b/255, alpha: 1.0)
    }
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat){
        self.init(red: r/255, green: g/255, blue: b/255, alpha: a)
    }
}
